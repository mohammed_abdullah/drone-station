import React from 'react';

class Registration extends React.Component {
    constructor(props) {
        super(props);
        this.state = { firstName: "", lastName: "", emergencyPhoneNumber: "", email: "", message: "" }
    }
    submitForm(e) {
        e.preventDefault();
        let flag = false;
        if (this.state.firstName !== "" && this.state.lastName !== "" && this.state.emergencyPhoneNumber !== "" && this.state.email !== "") {
            if (this.props.currentUsers.length !== 0) {
                this.props.currentUsers.forEach(user => {
                    if (this.state.email.toLowerCase() === user.email.toLowerCase()) {
                        flag = true;
                        return;
                    }
                });

            } else {
                this.setState({ message: "Registered Successfully" });
                this.props.addUser(this.state);
                console.log("state is added");


            }
            if (flag) {
                this.setState({ message: "Email already exists,try different email" });
            } else {
                this.setState({ message: "Registered Successfully" });
                this.props.addUser(this.state);

            }

        }


    }
    formPreventDefault(e) {
        e.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.formPreventDefault.bind(this)}>
                <div>Registration</div>
                <div><label>First Name</label>  <input type="text" value={this.state.firstName} onChange={event => this.setState({ firstName: event.target.value })} /></div>
                <div><label>Last Name</label>  <input type="text" value={this.state.lastName} onChange={event => this.setState({ lastName: event.target.value })} /></div>
                <div><label>Emergency Phone Number</label>  <input type="text" value={this.state.emergencyPhoneNumber} onChange={event => this.setState({ emergencyPhoneNumber: event.target.value })} /></div>
                <div><label>Email</label>  <input type="text" value={this.state.email} onChange={event => this.setState({ email: event.target.value })} /></div>
                <div>{this.state.message}</div>
                <button onClick={this.submitForm.bind(this)}> Register</button>
            </form>
        )
    }

}

export default Registration;