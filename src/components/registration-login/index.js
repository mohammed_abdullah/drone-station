
import React from 'react';
import Registration from './registrationComponent/registration';
import Login from './LoginComponent/login';

class RegistrationLogin extends React.Component {
    constructor(props){
    super(props);
    this.state = {
        users : [],
        isLogin: false,
        isRegistration: false
    }
    }
    addUser(user){
        console.log("Add user in prop works");
        
        let convertToState = this.state.users.concat(user);
        console.log(convertToState);
        
        this.setState({users:convertToState,isRegistration:false});
        
        
        }
    buttonClick(choice){
        if(choice === 1){
            this.setState({isLogin:true});
        }else{
            this.setState({isRegistration:true});
        }
    }

    userExist(value){
        if(value === 1){
            this.props.login(this.state.users);
            this.setState({isLogin: false})
            this.props.history.push('/');
        }
    }

render(){
    const chooseUi = <div><button onClick={this.buttonClick.bind(this,1)}>Login</button><button onClick={this.buttonClick.bind(this,2)}>Register</button></div>;
    const registration = <Registration history={this.props.history} addUser={this.addUser.bind(this)} currentUsers = {this.state.users}></Registration>;
    const login = <Login currentUsers = {this.state.users} userExist = {this.userExist.bind(this)}></Login>;
    return (
    <div>
<div>{  !this.state.isLogin && !this.state.isRegistration && chooseUi}</div>
   
     
    {this.state.isLogin?(login):(this.state.isRegistration?(registration):(null))}
        
        
    </div>
    
    )
}
    
    
}
export default RegistrationLogin;