import React from 'react';
import { setInterval } from 'timers';

class Timer extends React.Component {

    constructor(props) {
        super(props);
        this.state = { minutes: 10 }
    }
    interval = 0;

    tick() {
        this.setState({ minutes: this.state.minutes - 1 });
        if (this.state.minutes === 0) {
            clearInterval(this.interval._id);
            this.props.droneCrashed();
        }
    }
    componentDidMount() {
        let minString = this.props.maxTime;
        this.setState({ minutes: minString.substring(0, minString.length - 3) })
        console.log("this is the min string", minString.substring(0, minString.length - 2));

        this.interval = setInterval(this.tick.bind(this), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval._id);
    }
    render() {
        return (<span> {this.state.minutes}min left</span>)
    }
}

export default Timer;