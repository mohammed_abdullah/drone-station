import React from 'react';




class Drone extends React.Component {

    // constructor(props){
    // super(props);
    //     }

    rentHandler(drone) {
        this.props.selectedDrone(drone, this.props.station);
        // this.props.history.push('/registration-login');

    }

    render() {
        const listStationDrones = this.props.array.map((drone) =>
            <div key={drone.model}>{drone.model}{!this.props.rentStarted && <button onClick={this.rentHandler.bind(this, drone)}>Rent</button>}</div>
        )

        return (<div>{listStationDrones}</div>)

    }

}
export default Drone;