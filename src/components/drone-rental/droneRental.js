import React from 'react';
import './droneRental.css';
import Timer from '../timer/timer';

function DroneRental(props) {
    let droneCrashed = () => {
        console.log("Drone crashed!!!!");
        props.droneCrashed();

    }
    return (
        <div className="container">
            <h2>Drone Rented</h2>
            <p>Manufacturer : {props.rentedDrone.manufacturer}</p>
            <p>Model :{props.rentedDrone.model}</p>
            <p>Maximum flight time : {props.rentedDrone.maxFlightTime}</p>
            <Timer droneCrashed={droneCrashed.bind(this)} maxTime={props.rentedDrone.maxFlightTime} />

        </div>

    )

}

export default DroneRental;