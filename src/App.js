import React from 'react';
import './App.css';
import quads from './json-files/quads';
import Drone from './components/drone/drone'
import RegistrationLogin from './components/registration-login';
import DroneMethods from './droneMethods/droneMethods';
import DroneRental from './components/drone-rental/droneRental';

class App extends React.Component {
  helper = new DroneMethods();
  settingUserLogginfinished = false;
  constructor(props) {
    super(props)
    this.state = {
      station1: [],
      station2: [],
      station3: [],
      rentedDrones: [],
      selectedDrone: {},
      users: [],
      selectedStation: 0,
      isLoggedIn: false,
      showRegistrationLogin: false,
      showRental: false,
      isCurrentUserBanned: false,
      showBanMessage: false
    }
  }
  componentDidMount() {
    let droneList = quads.quads;
    let station1 = [];
    let station2 = [];
    let station3 = [];
    let ratio = Math.round(droneList.length / 3);
    let counter1 = 0, counter2 = 0, counter3 = 0;


    droneList.forEach(drone => {

      if (counter1 < 10 && counter1 < ratio) {
        station1.push(drone);
        counter1++;
      } else if (counter2 < 10 && counter2 < ratio) {
        station2.push(drone);
        counter2++;
      } else if (counter3 < 10 && counter3 < ratio) {
        station3.push(drone);
        counter3++;
      }
    });

    var toState1 = this.state.station1.concat(station1);
    var toState2 = this.state.station2.concat(station2);
    var toState3 = this.state.station3.concat(station3)


    this.setState({ station1: toState1, station2: toState2, station3: toState3 });

  }
  selectedDrone(drone, station) {
    this.setState({ selectedDrone: drone, selectedStation: station });
    if (!this.state.isLoggedIn && !this.state.isCurrentUserBanned) {
      this.setState({ showRegistrationLogin: true });
    } else {
      this.setState({ showBanMessage: true });
    }
    console.log(drone + " " + station);

  }
  setUserLogin = (users) => {
    this.setState({ users: this.state.users.concat(users) });
    if (this.state.selectedStation === 1) {
      this.helper.removeRentedFromMain(this.state.selectedDrone, this.state.station1).then(res => {
        this.setState({
          isLoggedIn: true,
          showRegistrationLogin: false,
          rentedDrones: this.helper.addToArray(this.state.selectedDrone, this.state.rentedDrones),
          station1: res,
          showRental: true

        })


      })
    } else if (this.state.selectedStation === 2) {
      this.helper.removeRentedFromMain(this.state.selectedDrone, this.state.station2).then(res => {
        this.setState({
          isLoggedIn: true,
          showRegistrationLogin: false,
          rentedDrones: this.helper.addToArray(this.state.selectedDrone, this.state.rentedDrones),
          station2: res,
          showRental: true

        })


      })
    } else {
      this.helper.removeRentedFromMain(this.state.selectedDrone, this.state.station3).then(res => {
        this.setState({
          isLoggedIn: true,
          showRegistrationLogin: false,
          rentedDrones: this.helper.addToArray(this.state.selectedDrone, this.state.rentedDrones),
          station3: res,
          showRental: true

        })


      })

    }

  }

  returnDrone = (value) => {
    if (value === 1) {
      if (this.state.station1.length >= 10) {
        console.log("Station is full");
        return
      }
      this.setState({
        station1: this.helper.addToArray(this.state.selectedDrone, this.state.station1),
        selectedDrone: "",
        isLoggedIn: false,
        selectedStation: 0,
        showRental: false

      })
    } else if (value === 2) {
      if (this.state.station2.length >= 10) {
        console.log("Station is full");
        return
      }
      this.setState({
        station2: this.helper.addToArray(this.state.selectedDrone, this.state.station2),
        selectedDrone: "",
        isLoggedIn: false,
        selectedStation: 0,
        showRental: false
      })
    } else {
      if (this.state.station3.length >= 10) {
        console.log("Station is full");
        return
      }
      this.setState({
        station3: this.helper.addToArray(this.state.selectedDrone, this.state.station3),
        selectedDrone: "",
        isLoggedIn: false,
        selectedStation: 0,
        showRental: false
      })
    }
  }
  dronecrashed = () => {

    this.setState({
      isCurrentUserBanned: true
    })
    this.returnDrone(this.state.selectedStation);

  }
  render() {

    var droneStations = (<div>
      <div>
        <p>First Station{this.state.showRental && <button onClick={this.returnDrone.bind(this, 1)}>return drone to this station</button>}</p>
        <Drone array={this.state.station1} selectedDrone={this.selectedDrone.bind(this)} station={1} rentStarted={this.state.showRental}></Drone>
      </div>
      <div>
        <p>Second Station{this.state.showRental && <button onClick={this.returnDrone.bind(this, 2)}>return drone to this station</button>}</p>
        <Drone array={this.state.station2} selectedDrone={this.selectedDrone.bind(this)} station={2} rentStarted={this.state.showRental}></Drone>
      </div>
      <div>
        <p>Third Station {this.state.showRental && <button onClick={this.returnDrone.bind(this, 3)}>return drone to this station</button>}</p>
        <Drone array={this.state.station3} selectedDrone={this.selectedDrone.bind(this)} station={3} rentStarted={this.state.showRental}></Drone>
      </div>

    </div>)
    var registrationLogin = <RegistrationLogin history={this.props.history} login={this.setUserLogin.bind(this)}></RegistrationLogin>;
    return (
      <div>
        {this.state.showRegistrationLogin ? registrationLogin : droneStations}
        {this.state.showRental && <DroneRental droneCrashed={this.dronecrashed.bind(this)} rentedDrone={this.state.selectedDrone} />}
        {this.state.showBanMessage && <p>You are banned from renting anymore drones</p>}
      </div>
    );
  }

}

export default App;
