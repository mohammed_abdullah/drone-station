import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import RegistrationLogin from './components/registration-login/index';
import Login from './components/registration-login/LoginComponent/login';
import Registration from './components/registration-login/registrationComponent/registration';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import * as serviceWorker from './serviceWorker';

const routing = (
    <Router>
<Switch>
    <Route exact path="/" component={App}/>
    <Route path="/registration-login" component={RegistrationLogin}/>
    <Route path="/login" component={Login}/>
    <Route path="/registration" component={Registration}/>
</Switch>
</Router>
)
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
